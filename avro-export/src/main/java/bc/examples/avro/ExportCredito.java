package bc.examples.avro;

import bc.creditos.Credito;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.specific.SpecificDatumWriter;

/**
 *
 * @author Víctor
 */
public class ExportCredito {
    
    public static void main(String[] args) throws IOException {
        Credito credito=new Credito();
        credito.setRfc("12235");
        credito.setCantidad(new BigDecimal("100.34"));
        credito.setInstitucionCrediticia("BBVA");
        credito.setTasaInteres(new BigDecimal("7.60"));
        credito.setNota("XYU");
        try(DataFileWriter writer=new DataFileWriter<>(new SpecificDatumWriter<>(Credito.class))) {
            writer.create(credito.getSchema(), new File("c:/temp/creditos.avro"));
            writer.append(credito);
        }
    }
    
}

