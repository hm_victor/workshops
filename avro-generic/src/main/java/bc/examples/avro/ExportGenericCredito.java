package bc.examples.avro;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import org.apache.avro.Schema;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;

/**
 *
 * @author Víctor
 */
public class ExportGenericCredito {
    
    public static void main(String[] args) throws IOException {
        Schema schema = new Schema.Parser().parse(new File("../avro-export/src/main/avro/credito.avsc"));
        GenericRecord credito=new GenericData.Record(schema);
        credito.put("rfc", "12235");
        credito.put("cantidad", 100.34);
        credito.put("institucionCrediticia", "BBVA");
        credito.put("tasaInteres", 7.60);
        credito.put("nota", "XYU");
        try(DataFileWriter<GenericRecord> dataFileWriter = new DataFileWriter<>(new GenericDatumWriter<>(schema))) {
            dataFileWriter.create(schema, new File("c:/temp/creditosGenericos.avro"));
            dataFileWriter.append(credito);
        }
    }
    
}
