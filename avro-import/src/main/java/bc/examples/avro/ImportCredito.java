package bc.examples.avro;

import java.io.File;
import java.io.IOException;
import org.apache.avro.Schema;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.apache.avro.specific.SpecificDatumReader;

/**
 *
 * @author Víctor
 */
public class ImportCredito {

    public static void main(String[] args) throws IOException {
        Schema schema = new Schema.Parser().parse(new File("src/main/avro/credito.avsc"));
        DatumReader<GenericRecord> userDatumReader = new SpecificDatumReader<>(schema);
        try(DataFileReader<GenericRecord> reader = new DataFileReader<>(new File("c:/temp/creditos.avro"), userDatumReader)) {
            while(reader.hasNext()) {
                GenericRecord credito = reader.next();
                System.out.println("CREDITO: "+credito);
            }
        }
    }

}
