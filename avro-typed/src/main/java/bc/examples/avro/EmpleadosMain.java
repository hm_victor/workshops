package bc.examples.avro;

import bc.empleados.Empleado;
import bc.empleados.Empresa;
import bc.empleados.Puesto;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.specific.SpecificDatumWriter;

/**
 *
 * @author Víctor
 */
public class EmpleadosMain {
    
    public static void main(String[] args) throws IOException {
        Empleado empleado=new Empleado();
        empleado.setNombre("Alberto");
        empleado.setEdad(34);
        empleado.setPuesto(Puesto.OPERATIVO);
        empleado.setTelefonos(Arrays.asList("5528642290", "5537228890"));
        Empresa empresa=new Empresa();
        empresa.setNombre("Buró de Crédito");
        empresa.setRfc("BFGRT123344GHYT");
        empleado.setEmpresa(empresa);
        try(DataFileWriter writer=new DataFileWriter(new SpecificDatumWriter<>(Empleado.class))){
            writer.create(empleado.getSchema(), new File("c:/temp/empleados.avro"));
            writer.append(empleado);
        }
    }
    
}
