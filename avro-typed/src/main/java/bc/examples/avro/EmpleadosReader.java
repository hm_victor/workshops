package bc.examples.avro;

import bc.empleados.Empleado;
import java.io.File;
import java.io.IOException;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.specific.SpecificDatumReader;

/**
 *
 * @author Víctor
 */
public class EmpleadosReader {
    
    public static void main(String[] args) throws IOException {
        try(DataFileReader<Empleado> reader=new DataFileReader<>(new File("c:/temp/empleados.avro"), new SpecificDatumReader<>(Empleado.class))) {
            while(reader.hasNext()) {
                Empleado empleado = reader.next();
                System.out.println("EMPLEADO: "+empleado);
            }
        }
    }
    
}
