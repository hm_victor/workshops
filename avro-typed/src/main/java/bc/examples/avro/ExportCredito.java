package bc.examples.avro;

import bc.creditos.Credito;
import java.io.File;
import java.io.IOException;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.specific.SpecificDatumWriter;

/**
 *
 * @author Víctor
 */
public class ExportCredito {
    
    public static void main(String[] args) throws IOException {
        Credito credito1=new Credito();
        credito1.setRfc("ERTYU789765JK9");
        credito1.setCantidad(100.34);
        credito1.setInstitucionCrediticia("BBVA");
        credito1.setTasaInteres(7.60);
        credito1.setNota("Qwerty");
        
        Credito credito2=new Credito();
        credito2.setRfc("DCVBNJ89765QW5");
        credito2.setCantidad(15_000);
        credito2.setInstitucionCrediticia("Banorte");
        credito2.setTasaInteres(8.1);
        credito2.setNota("General");
        
        Credito credito3=new Credito();
        credito3.setRfc("KIYTV345678ZX2");
        credito3.setCantidad(1_000_000);
        credito3.setInstitucionCrediticia("HSBC");
        credito3.setTasaInteres(5);
        
        try(DataFileWriter writer=new DataFileWriter<>(new SpecificDatumWriter<>(Credito.class))) {
            writer.create(credito1.getSchema(), new File("c:/temp/creditos.avro"));
            writer.append(credito1);
            writer.append(credito2);
            writer.append(credito3);
        }
    }
    
}

