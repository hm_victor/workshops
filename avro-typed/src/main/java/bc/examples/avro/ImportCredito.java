package bc.examples.avro;

import bc.creditos.Credito;
import java.io.File;
import java.io.IOException;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.io.DatumReader;
import org.apache.avro.specific.SpecificDatumReader;

/**
 *
 * @author Víctor
 */
public class ImportCredito {

    public static void main(String[] args) throws IOException {
        DatumReader<Credito> userDatumReader = new SpecificDatumReader<>(Credito.class);
        try(DataFileReader<Credito> reader = new DataFileReader<>(new File("c:/temp/creditos.avro"), userDatumReader)) {
            while(reader.hasNext()) {
                Credito credito = reader.next();
                System.out.println("CREDITO: "+credito);
            }
        }
    }

}
