package org.example.pmml;

import java.util.HashMap;
import java.util.Map;
import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.Results;
import org.kie.api.io.ResourceType;
import org.kie.api.pmml.PMML4Result;
import org.kie.api.pmml.PMMLRequestData;
import org.kie.api.runtime.KieContainer;
import org.kie.pmml.pmml_4_2.PMML4ExecutionHelper;
import org.kie.pmml.pmml_4_2.PMML4ExecutionHelper.PMML4ExecutionHelperFactory;

/**
 *
 * @author Víctor
 */
public class MainSampleScorecard {

    public static void main(String[] args) {
        KieServices kieServices = KieServices.Factory.get();
        KieFileSystem kfs = kieServices.newKieFileSystem();
        kfs.write(kieServices.getResources().newUrlResource(MainSampleScorecard.class.getResource("/pmml/scorecard.xml"))
                .setSourcePath("/pmml/scorecard.xml")
                .setResourceType(ResourceType.PMML));
        System.out.println("PMML Score card translated.");
        KieBuilder kieBuilder = kieServices.newKieBuilder(kfs);

        Results res = kieBuilder.buildAll().getResults();
        res.getMessages().forEach(message -> {
            System.out.println(message.getLevel()+" "+message.toString());
        });
        KieContainer kieContainer = kieServices.newKieContainer( kieBuilder.getKieModule().getReleaseId() );
        
        
        KieBase kBase = kieContainer.getKieBase();
        HashMap<String, Object> variables = new HashMap<>();
        variables.put("department", "marketing");
        variables.put("age", 29);
        variables.put("income", 1000d);
        variables.put("income", 1d);
        executeModel(kBase, variables, "SampleScorecard", "packageName", "123");
        
    }

    public static void executeModel(KieBase kbase, Map<String, Object> variables, String modelName, String modelPkgName, String correlationId) {
        PMML4ExecutionHelper helper = PMML4ExecutionHelperFactory.getExecutionHelper(modelName, kbase);
        helper.addPossiblePackageName(modelPkgName);

        PMMLRequestData request = new PMMLRequestData(correlationId, modelName);
        variables.entrySet().forEach(entry -> {
            request.addRequestParam(entry.getKey(), entry.getValue());
        });

        PMML4Result resultHolder = helper.submitRequest(request);
        if ("OK".equals(resultHolder.getResultCode())) {
            resultHolder.getResultVariables().forEach((name, value) -> {
                System.out.printf("%s -> %s%n", name, value);
            });
        }
    }

}
