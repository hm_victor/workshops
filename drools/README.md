# Instalación de Drools en Wildfly 

## Etapa 1: Preparación

1. Configurar los usuarios utilizados para la aplicación
    1. Usuario administrador de Wildfly (Para acceder a la consola y realizar o verificar el despliegue manualmente).
    2. Usuario del workbench (Para acceder manualmente y realizar labores de administración de las reglas de Drools).
    3. Usuario del servidor de configuración (Utilizado para la interconexión del workbench y el servidor de ejecución de Drools).

## Etapa 2: Arranque

2. Establecer las opciones de Java
    1. Configurar la memoria suficiente. 
        `-Xms1024m -Xmx3000m -XX:MetaspaceSize=512M -XX:MaxMetaspaceSize=512m -XX:+UseAdaptiveSizePolicy`
    2. Aumentar el tiempo de timeout de conexión entre servicios.
        `-Djboss.as.management.blocking.timeout=3600`
    
3. Parámetros de inicio de Wildfly (Al invocar el archivo %WILDFLY_HOME%\bin\standalone.bat).
    1. Asegurarse de utilizar el perfil completo del servidor.
        `-c standalone-full.xml`
    2. Configurar las ip de escucha.
        `-b 0.0.0.0 -bmanagement 0.0.0.0`
    3. Configurar las urls de las aplicaciones de Drools.
        `-Dorg.kie.server.id=wildfly-kieserver`
        `-Dorg.kie.server.controller=http://localhost:8080/kie-wb/rest/controller`
        `-Dorg.kie.server.location=http://localhost:8080/kie-server/services/rest/server`
    4. Configurar los accesos utilizados para la intercomunicación entre las aplicaciones. Estos accesos son los configurados en la Etapa 1.
        `-Dorg.kie.workbench.controller.user=kieserver`
        `-Dorg.kie.workbench.controller.pwd=kieserver#123`
        `-Dorg.kie.server.user=kieserver`
        `-Dorg.kie.server.pwd=kieserver#123`

## Etapa 3: Despliegue

4. Despliegue de los wars de las aplicaciones.

Para comprobar que le instalación es exitosa, en el navegador entrar a la url `http://localhost:8080/kie-server/services/rest/server`. Introducir las credenciales del usuario `kieserver`.

Se debe mostrar una salida como la siguiente.

![Respuesta de Kie Server](kie-server.png)

## Scripts

En los siguientes archivos de script se automatizan estos pasos.

1. installDrools.cmd (Etapa 1)
2. startWildflyDrools.cmd (Etapa 2)
3. deploy.cmd (Etapa 3)