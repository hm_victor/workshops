export WILDFLY_HOME=/opt/jboss/wildfly

$WILDFLY_HOME/bin/jboss-cli.sh --connect --commands="deploy --force docker/kie-wb/kie-wb.war, deploy --force docker/kie-wb/kie-server.war"

