export WILDFLY_HOME=/opt/jboss/wildfly

echo $WILDFLY_HOME

$WILDFLY_HOME/bin/add-user.sh admin Admin#123 --silent
$WILDFLY_HOME/bin/add-user.sh -a -u kieserver -p kieserver#123 -g kie-server
$WILDFLY_HOME/bin/add-user.sh -a -u kiewb -p kiewb#123 -g admin,kie-server
