export WILDFLY_HOME=/opt/jboss/wildfly

export JAVA_OPTS=-server -Xms1024m -Xmx3000m -XX:MetaspaceSize=512M -XX:MaxMetaspaceSize=512m -XX:+UseAdaptiveSizePolicy -Djboss.modules.system.pkgs=org.jboss.byteman -Djava.awt.headless=true -Djava.net.preferIPv4Stack=true -Djboss.as.management.blocking.timeout=3600

$WILDFLY_HOME/bin/standalone.sh -c standalone-full.xml -b 0.0.0.0 -bmanagement 0.0.0.0  \
        -Dorg.kie.server.id=wildfly-kieserver  \
        -Dorg.kie.server.controller=http://localhost:8080/kie-wb/rest/controller  \
        -Dorg.kie.server.location=http://localhost:8080/kie-server/services/rest/server  \
        -Dorg.kie.workbench.controller.user=kieserver  \
        -Dorg.kie.workbench.controller.pwd=kieserver#123  \
        -Dorg.kie.server.user=kieserver  \
        -Dorg.kie.server.pwd=kieserver#123
