package bc.hadoop.csv;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

/**
 *
 * @author Víctor
 */
public class FileReader {

    public static void main(String[] args) throws IOException {
        Configuration conf = new Configuration();
        try ( FileSystem fileSystem = FileSystem.get(conf)) {
            Path path = new Path("/mycsvs/COVID19-MEXICO.csv");
            if (fileSystem.exists(path)) {
                try ( BufferedReader reader = new BufferedReader(new InputStreamReader(fileSystem.open(path)))) {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        System.out.println(line);
                    }
                }
            } else {
                System.err.println("File " + path + " does not exists");
            }
        }
    }

}
