package bc.hadoop.csv;

import java.io.IOException;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
//import org.apache.hadoop.mapred.FileInputFormat;
//import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.reduce.IntSumReducer;

/**
 *
 * @author Víctor
 */
public class LineCounter {

    public static class RegisterCounter extends Mapper<Object, Text, Text, IntWritable> {

        private final static IntWritable ONE = new IntWritable(1);
        private final static IntWritable ZERO = new IntWritable(0);
        private Text text = new Text();

        @Override
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            if(value.toString().contains("FECHA_ACTUALIZACION")) {
                text.set(ZERO.toString());
                context.write(text, ZERO);
            }else{
                text.set(ONE.toString());
                context.write(text, ONE);
            }
        }
        
    }

    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        JobConf jobConf=new JobConf();

        Job job = Job.getInstance(jobConf, "line count");
        job.setJarByClass(LineCounter.class);
        job.setMapperClass(RegisterCounter.class);
        job.setCombinerClass(IntSumReducer.class);
        job.setReducerClass(IntSumReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        FileInputFormat.addInputPath(job, new Path("/mycsvs"));
        FileOutputFormat.setOutputPath(job, new Path("/outputCvs"));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }

}
