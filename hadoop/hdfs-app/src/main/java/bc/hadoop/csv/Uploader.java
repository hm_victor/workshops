package bc.hadoop.csv;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

/**
 *
 * @author Víctor
 */
public class Uploader {

    public static void main(String[] args) throws IOException {
        Configuration conf = new Configuration();
        try(FileSystem fileSystem = FileSystem.get(conf)){
            Path path = new Path("/mycsvs/COVID19-MEXICO.csv");
            if (fileSystem.exists(path)) {
                System.out.println("File " + path + " already exists");
                return;
            }
            try(FSDataOutputStream out = fileSystem.create(path);
                InputStream in = new BufferedInputStream(new FileInputStream(new File("/COVID19-MEXICO.csv")))) {
                byte[] b = new byte[1024];
                int numBytes;
                while ((numBytes = in.read(b)) > 0) {
                    out.write(b, 0, numBytes);
                }
            }
        }
    }

}
