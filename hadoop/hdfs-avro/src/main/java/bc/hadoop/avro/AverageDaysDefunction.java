package bc.hadoop.avro;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import mx.ssa.RecordCovid;
import org.apache.avro.Schema;
import org.apache.avro.mapred.AvroKey;
import org.apache.avro.mapred.AvroValue;
import org.apache.avro.mapreduce.AvroJob;
import org.apache.avro.mapreduce.AvroKeyInputFormat;
import org.apache.avro.mapreduce.AvroKeyValueOutputFormat;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BooleanWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 *
 * @author Víctor
 */
public class AverageDaysDefunction {

    public static class CovidMapper extends Mapper<AvroKey<RecordCovid>, NullWritable, BooleanWritable, DoubleWritable> {
        private BooleanWritable aliveWritable=new BooleanWritable();
        private DoubleWritable numberOfDaysWritable=new DoubleWritable();

        @Override
        protected void map(AvroKey<RecordCovid> key, NullWritable value, Context context) throws IOException, InterruptedException {
            if(key.datum().getFECHADEF() < 0L) {
                aliveWritable.set(true);
                numberOfDaysWritable.set(-1L);
            }else{
                aliveWritable.set(false);
                numberOfDaysWritable.set(ChronoUnit.DAYS.between(
                                            Instant.ofEpochMilli(key.datum().getFECHASINTOMAS()).atZone(ZoneId.systemDefault()).toLocalDate(),
                                            Instant.ofEpochMilli(key.datum().getFECHADEF()).atZone(ZoneId.systemDefault()).toLocalDate()));
            }
            context.write(aliveWritable, numberOfDaysWritable);
        }

    }

    public static class CovidCombiner extends Reducer<BooleanWritable, DoubleWritable, BooleanWritable, DoubleWritable> {
        private DoubleWritable averageWritable=new DoubleWritable();

        @Override
        protected void reduce(BooleanWritable key, Iterable<DoubleWritable> collected, Context context) throws IOException, InterruptedException {
            if(key.get()){
                averageWritable.set(-1d);
            }else{
                long counter=0;
                double sum=0;
                for (DoubleWritable numberOfDays : collected) {
                    counter++;
                    sum+=numberOfDays.get();
                }
                averageWritable.set(counter == 0 ? 0: sum/counter);
            }
            context.write(key, averageWritable);
        }
        
    }

    public static class CovidReducer extends Reducer<BooleanWritable, DoubleWritable, AvroKey<Boolean>, AvroValue<Double>> {
        private AvroKey<Boolean> aliveKey=new AvroKey<>();
        private AvroValue<Double> averageValue=new AvroValue<>();

        @Override
        protected void reduce(BooleanWritable key, Iterable<DoubleWritable> values, Context context) throws IOException, InterruptedException {
            aliveKey.datum(key.get());
            if(key.get()) {
                averageValue.datum(-1d);
            }else{
                long counter=0;
                double sum=0;
                for (DoubleWritable value : values) {
                    counter++;
                    sum+=value.get();
                }
                averageValue.datum(counter == 0? 0: sum/counter);
            }
            context.write(aliveKey, averageValue);
        }
        
    }

    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        JobConf jobConf = new JobConf();

        Job job = Job.getInstance(jobConf, "Average days");

        job.setJarByClass(AverageDaysDefunction.class);
        job.setJobName("Defunction average days");

//        String[] args = new GenericOptionsParser(rawArgs).getRemainingArgs();
        Path inPath = new Path("/avroFiles");
        Path outPath = new Path("/outputAvro");

        FileInputFormat.setInputPaths(job, inPath);
        FileOutputFormat.setOutputPath(job, outPath);

        outPath.getFileSystem(jobConf).delete(outPath, true);

        job.setInputFormatClass(AvroKeyInputFormat.class);
        job.setMapperClass(CovidMapper.class);

        AvroJob.setInputKeySchema(job, RecordCovid.getClassSchema());
        job.setMapOutputKeyClass(BooleanWritable.class);
        job.setMapOutputValueClass(DoubleWritable.class);

        job.setCombinerClass(CovidCombiner.class);

        job.setOutputFormatClass(AvroKeyValueOutputFormat.class);
        job.setReducerClass(CovidReducer.class);
        AvroJob.setOutputKeySchema(job, Schema.create(Schema.Type.BOOLEAN));
        AvroJob.setOutputValueSchema(job, Schema.create(Schema.Type.DOUBLE));

        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }

}
