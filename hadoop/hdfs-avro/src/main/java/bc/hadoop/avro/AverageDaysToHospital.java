package bc.hadoop.avro;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import mx.ssa.RecordCovid;
import org.apache.avro.Schema;
import org.apache.avro.mapred.AvroKey;
import org.apache.avro.mapreduce.AvroJob;
import org.apache.avro.mapreduce.AvroKeyInputFormat;
import org.apache.avro.mapreduce.AvroKeyValueOutputFormat;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BooleanWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 *
 * @author Víctor
 */
public class AverageDaysToHospital {
    
    public static class DaysToHospitalMapper extends Mapper<AvroKey<RecordCovid>, NullWritable, BooleanWritable, DoubleWritable> {

        private BooleanWritable aliveWritable=new BooleanWritable();
        private DoubleWritable numberOfDaysWritable=new DoubleWritable();
        
        @Override
        protected void map(AvroKey<RecordCovid> key, NullWritable value, Context context) throws IOException, InterruptedException {
            if(key.datum().getFECHADEF() >= 0L) {
                LocalDate fechaSintomas = Instant.ofEpochMilli(key.datum().getFECHASINTOMAS()).atZone(ZoneId.systemDefault()).toLocalDate();
                LocalDate fechaIngresoHospital = Instant.ofEpochMilli(key.datum().getFECHAINGRESO()).atZone(ZoneId.systemDefault()).toLocalDate();
                if(fechaIngresoHospital.compareTo(fechaSintomas) >= 0){
                    aliveWritable.set(false);
                    numberOfDaysWritable.set(ChronoUnit.DAYS.between(fechaSintomas, fechaIngresoHospital));
                    context.write(aliveWritable, numberOfDaysWritable);
                }
            }
        }

    }
    
    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        JobConf jobConf = new JobConf();

        Job job = Job.getInstance(jobConf, "Average days to hospital");

        job.setJarByClass(AverageDaysDefunction.class);
        job.setJobName("Hospitalization average days");

//        String[] args = new GenericOptionsParser(rawArgs).getRemainingArgs();
        Path inPath = new Path("/avroFiles");
        Path outPath = new Path("/outputAvroHospital");

        FileInputFormat.setInputPaths(job, inPath);
        FileOutputFormat.setOutputPath(job, outPath);

        outPath.getFileSystem(jobConf).delete(outPath, true);

        job.setInputFormatClass(AvroKeyInputFormat.class);
        job.setMapperClass(DaysToHospitalMapper.class);

        AvroJob.setInputKeySchema(job, RecordCovid.getClassSchema());
        job.setMapOutputKeyClass(BooleanWritable.class);
        job.setMapOutputValueClass(DoubleWritable.class);

        job.setCombinerClass(AverageDaysDefunction.CovidCombiner.class);

        job.setOutputFormatClass(AvroKeyValueOutputFormat.class);
        job.setReducerClass(AverageDaysDefunction.CovidReducer.class);
        AvroJob.setOutputKeySchema(job, Schema.create(Schema.Type.BOOLEAN));
        AvroJob.setOutputValueSchema(job, Schema.create(Schema.Type.DOUBLE));

        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
    
}
