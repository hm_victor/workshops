package bc.hadoop.avro;

/**
 *
 * @author Víctor
 */
public class Column {
    
    private final String name;
    private final int index;
    private Converter<?> converter;

    public Column(String name, int index) {
        this.name = name;
        this.index = index;
    }

    public Column(String name, int index, Converter<?> converter) {
        this.name = name;
        this.index = index;
        this.converter = converter;
    }
    
    public Converter<?> getConverter() {
        return converter;
    }

    public void setConverter(Converter<?> converter) {
        this.converter = converter;
    }

    public static int getCounter() {
        return counter;
    }

    public static void setCounter(int counter) {
        Column.counter = counter;
    }
    
    public String getName() {
        return name;
    }

    public int getIndex() {
        return index;
    }

    public Object getValue(String[] row) throws Exception {
        return converter == null? row[index]: converter.get(row[index]);
    }
    private static int counter = 0;

    public static Column create(String name) {
        return new Column(name, counter++);
    }
    
    public static <T> Column create(String name, Converter<T> converter) {
        return new Column(name, counter++, converter);
    }
    
}
