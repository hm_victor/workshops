package bc.hadoop.avro;

/**
 *
 * @author Víctor
 */
public interface Converter<T> {
    
    public T get(String value) throws Exception ;
    
}
