package bc.hadoop.avro;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import mx.ssa.RecordCovid;
import org.apache.avro.Schema;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

/**
 *
 * @author Víctor
 */
public class CsvToAvroUploader {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    public static void main(String[] args) throws IOException, Exception {
        Column[] columns = new Column[]{
            Column.create("FECHA_ACTUALIZACION"),
            Column.create("ID_REGISTRO"),
            Column.create("ORIGEN"),
            Column.create("SECTOR"),
            Column.create("ENTIDAD_UM"),
            Column.create("SEXO"),
            Column.create("ENTIDAD_NAC"),
            Column.create("ENTIDAD_RES"),
            Column.create("MUNICIPIO_RES"),
            Column.create("TIPO_PACIENTE"),
            Column.create("FECHA_INGRESO", new DateConverter()),
            Column.create("FECHA_SINTOMAS", new DateConverter()),
            Column.create("FECHA_DEF", new Converter<Long>() {

                @Override
                public Long get(String value) throws ParseException {
                    if (value.contains("9999-99-99")) {
                        return -1L;
                    }
                    return DATE_FORMAT.parse(value.replace("\"", "")).getTime();
                }

            }),
            Column.create("INTUBADO"),
            Column.create("NEUMONIA"),
            Column.create("EDAD"),
            Column.create("NACIONALIDAD"),
            Column.create("EMBARAZO"),
            Column.create("HABLA_LENGUA_INDIG"),
            Column.create("DIABETES"),
            Column.create("EPOC"),
            Column.create("ASMA"),
            Column.create("INMUSUPR"),
            Column.create("HIPERTENSION"),
            Column.create("OTRA_COM"),
            Column.create("CARDIOVASCULAR"),
            Column.create("OBESIDAD"),
            Column.create("RENAL_CRONICA"),
            Column.create("TABAQUISMO"),
            Column.create("OTRO_CASO"),
            Column.create("RESULTADO"),
            Column.create("MIGRANTE"),
            Column.create("PAIS_NACIONALIDAD"),
            Column.create("PAIS_ORIGEN"),
            Column.create("UCI")
        };
        Configuration conf = new Configuration();
        Schema schema = RecordCovid.getClassSchema();
        try ( FileSystem fileSystem = FileSystem.get(conf)) {
            Path path = new Path("/avroFiles/COVID19-MEXICO.avro");
            if (fileSystem.exists(path)) {
                System.out.println("File " + path + " already exists");
                return;
            }
            try ( DataFileWriter<GenericRecord> dataFileWriter = new DataFileWriter<>(new GenericDatumWriter<>(schema));  BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("/COVID19-MEXICO.csv")))) {
                dataFileWriter.create(schema, fileSystem.create(path));
                String line;
                while ((line = reader.readLine()) != null) {
                    if (!line.contains(columns[0].getName())) {
                        String[] tokens = line.split(",");
                        GenericRecord record = new GenericData.Record(schema);
                        for (Column column : columns) {
                            record.put(column.getName(), column.getValue(tokens));
                        }
                        dataFileWriter.append(record);
                    }
                }
            }
        }
    }

    private static class DateConverter implements Converter<Long> {

        @Override
        public Long get(String value) throws ParseException {
            return DATE_FORMAT.parse(value.replace("\"", "")).getTime();
        }

    }

}
