package bc.hamcrest;

/**
 *
 * @author Víctor
 */
public class A {

    public void someMethod(Object object) {
        if(object == null) {
            throw new IllegalArgumentException();
        }
    }
    
}
