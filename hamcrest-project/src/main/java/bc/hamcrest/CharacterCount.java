package bc.hamcrest;

/**
 *
 * @author Víctor
 */
public class CharacterCount {
    
    public static int count(String value) {
        return value== null ? 0: value.length();
    }
    
}
