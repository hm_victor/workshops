package bc.hamcrest;

/**
 *
 * @author Víctor
 */
public class Duplicator {
    
    public Integer duplicate(int a) {
        return a * 2;
    }
    
}
