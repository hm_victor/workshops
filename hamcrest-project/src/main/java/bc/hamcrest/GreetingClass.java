package bc.hamcrest;

/**
 *
 * @author Víctor
 */
public class GreetingClass {
    
    public String greeting(String name) {
        return "Hello, "+name+"!";
    }
    
}
