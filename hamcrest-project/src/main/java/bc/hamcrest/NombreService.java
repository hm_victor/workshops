package bc.hamcrest;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Víctor
 */
public class NombreService {
    
    public List<String> getNombres() {
        return Arrays.asList("Pedro", "Juan", "Federico", "Alicia", "María");
    }
    
}
