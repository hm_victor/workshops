package bc.hamcrest;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;
import org.junit.Test;

/**
 *
 * @author Víctor
 */
public class ATest {
    
    @Test
    public void shouldThrowException() {
        A a=new A();
        try{
            a.someMethod(null);
            fail();
        }catch(Exception ex) {
            assertThat(ex, instanceOf(IllegalArgumentException.class));
        }
    }
    
}
