package bc.hamcrest;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author Víctor
 */
@RunWith(Parameterized.class)
public class CharacterCountTest {
    private String stringToCount;
    private int expectedCount;

    public CharacterCountTest(String stringToCount, int expectedCount) {
        this.stringToCount = stringToCount;
        this.expectedCount = expectedCount;
    }
    
    @Test
    public void shouldHaveCount() {
        assertThat(CharacterCount.count(stringToCount), is(expectedCount));
    }
    
    @Parameterized.Parameters
    public static Object[][] getData() {
        return new Object[][]{
            {"hello", 5},
            {"abc", 3},
            {"a", 1},
            {"", 0},
            {null, 0}    
        };
    }
    
}

