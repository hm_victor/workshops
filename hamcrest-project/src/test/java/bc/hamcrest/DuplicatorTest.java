package bc.hamcrest;

import static bc.hamcrest.ModuleMatcher.module;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import org.junit.Test;

/**
 *
 * @author Víctor
 */
public class DuplicatorTest {
    
    @Test
    public void shouldDuplicate() {
        Duplicator duplicator=new Duplicator();
        Integer result = duplicator.duplicate(3);
        assertThat(result, allOf(
                            notNullValue(), 
                            greaterThan(0),
                            module(2, 0)));
    }
    
}
