package bc.hamcrest;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.Test;

/**
 *
 * @author Víctor
 */
public class GreetingClassTest {
    
    @Test
    public void shouldGreet() {
        GreetingClass g=new GreetingClass();
        assertThat(g.greeting("Beto"), is("Hello, Beto!"));
    }
    
    @Test
    public void shouldGreetToo() {
        GreetingClass g=new GreetingClass();
        assertThat(g.greeting("Beto"), equalTo("Hello, Beto!"));
    }
    
}
