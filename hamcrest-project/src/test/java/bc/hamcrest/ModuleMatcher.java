package bc.hamcrest;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

/**
 *
 * @author Víctor
 */
public class ModuleMatcher extends TypeSafeMatcher<Integer> {
    private final int divider;
    private final int expected;

    public ModuleMatcher(int divider, int expected) {
        this.divider = divider;
        this.expected = expected;
    }

    @Override
    protected boolean matchesSafely(Integer t) {
        return t % divider == expected;
    }

    @Override
    public void describeTo(Description d) {
        d.appendText("module ").appendValue(divider).appendText(" equal to ").appendValue(expected);
    }

    @Override
    protected void describeMismatchSafely(Integer item, Description mismatchDescription) {
        mismatchDescription.appendText("was ").appendValue(item).appendText(" module ").appendValue(divider).appendText(" eq ").appendValue(item%divider);
    }
    
    public static ModuleMatcher module(int divider, int expected) {
        return new ModuleMatcher(divider, expected);
    }
    
}
