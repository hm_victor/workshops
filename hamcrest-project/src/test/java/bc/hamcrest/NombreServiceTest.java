package bc.hamcrest;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.Test;

/**
 *
 * @author Víctor
 */
public class NombreServiceTest {
    
    @Test
    public void shouldHaveName() {
        NombreService service=new NombreService();
        assertThat(service.getNombres(), hasItems("Pedro", "Juan", "Guadalupe"));
    }
    
}

