package bc.examples.mockito.spy;

/**
 *
 * @author Víctor
 */
public class Replacer {
    private Reverser spiedClass;

    public void setSpiedClass(Reverser spiedClass) {
        this.spiedClass = spiedClass;
    }
    
    public void execute(String value) {
        StringBuilder builder=new StringBuilder();
        value.chars().forEach(letter -> {
            if(letter == 'a' || letter == 'e' || letter == 'i' || letter == 'o' || letter == 'u') {
                builder.append((char)Character.toUpperCase(letter));
            }else{
                builder.append((char)letter);
            }
        });
        spiedClass.reverse(builder.toString());
    }
    
}
