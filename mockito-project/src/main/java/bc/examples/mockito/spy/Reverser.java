package bc.examples.mockito.spy;

/**
 *
 * @author Víctor
 */
public class Reverser {

    public void reverse(String value) {
        System.out.println(new StringBuilder(value).reverse().toString());
    }
    
}
