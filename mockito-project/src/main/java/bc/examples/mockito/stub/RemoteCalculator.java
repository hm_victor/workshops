package bc.examples.mockito.stub;

/**
 *
 * @author Víctor
 */
public interface RemoteCalculator {
    
    int getResult(int a);
    
}
