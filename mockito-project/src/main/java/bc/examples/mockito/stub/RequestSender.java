package bc.examples.mockito.stub;

/**
 *
 * @author Víctor
 */
public class RequestSender {
    private RemoteCalculator remoteCalculator;

    public void setRemoteCalculator(RemoteCalculator remoteCalculator) {
        this.remoteCalculator = remoteCalculator;
    }
    
    public int send(int value) {
        int result = remoteCalculator.getResult(value);
        return result * result;
    }
    
}
