package bc.examples.mockito.verify;

/**
 *
 * @author Víctor
 */
public interface RemotePrinter {

    void print(String text);
    
}
