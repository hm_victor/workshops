package bc.examples.mockito.verify;

/**
 *
 * @author Víctor
 */
public class ServiceA {
    private RemotePrinter printer;

    public void setPrinter(RemotePrinter printer) {
        this.printer = printer;
    }
    
    public void executeSum(int a, int b) {
        printer.print(String.valueOf(a + b));
    }
    
}
