package bc.examples.mockito.project;

import bc.examples.mockito.spy.Replacer;
import bc.examples.mockito.spy.Reverser;
import org.junit.Test;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.spy;

/**
 *
 * @author Víctor
 */
public class SpyTest {
    
    @Test
    public void deberiaPasarPrueba() {
        Replacer replacer=new Replacer();
        Reverser reverser=spy(new Reverser());
        replacer.setSpiedClass(reverser);
        replacer.execute("abcd");
        then(reverser).should().reverse("Abcd");
    }
    
}
