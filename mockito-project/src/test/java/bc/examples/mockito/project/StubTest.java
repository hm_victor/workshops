package bc.examples.mockito.project;

import bc.examples.mockito.stub.RequestSender;
import bc.examples.mockito.stub.RemoteCalculator;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.BDDMockito.given;

/**
 *
 * @author Víctor
 */
public class StubTest {
    
    @Test
    public void deberiaObtenerResultado() {
        RemoteCalculator remoteCalculator = mock(RemoteCalculator.class);
        
        RequestSender requestSender=new RequestSender();
        requestSender.setRemoteCalculator(remoteCalculator);
        
        given(remoteCalculator.getResult(7)).willReturn(14);
        
        assertThat(requestSender.send(7), is(14*14));
    }
    
}
