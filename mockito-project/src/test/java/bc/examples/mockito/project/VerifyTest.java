package bc.examples.mockito.project;

import bc.examples.mockito.verify.RemotePrinter;
import bc.examples.mockito.verify.ServiceA;
import org.junit.Test;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

/**
 *
 * @author Víctor
 */
public class VerifyTest {
    
    @Test
    public void deberiaVerificarImpresion() {
        ServiceA service=new ServiceA();
        RemotePrinter remotePrinter = mock(RemotePrinter.class);
        service.setPrinter(remotePrinter);
        
        service.executeSum(15, 78);
        then(remotePrinter).should().print("93");
    }
    
}
