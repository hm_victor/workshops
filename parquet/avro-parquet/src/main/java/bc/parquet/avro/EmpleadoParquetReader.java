package bc.parquet.avro;

import java.io.IOException;
import org.apache.avro.generic.GenericData;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.parquet.avro.AvroParquetReader;
import org.apache.parquet.hadoop.ParquetReader;

/**
 *
 * @author Víctor
 */
public class EmpleadoParquetReader {

    public static void main(String[] args) throws IOException {
        Path path = new Path("/empleado.parquet");
        try ( ParquetReader reader = AvroParquetReader
                .builder(path)
                .withConf(new Configuration())
                .build()) {
            GenericData.Record record;
            while ((record = (GenericData.Record) reader.read()) != null) {
                System.out.println(record);
            }
        }
    }

}
