package bc.parquet.avro;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.parquet.avro.AvroParquetWriter;
import org.apache.parquet.hadoop.ParquetWriter; 
import org.apache.parquet.hadoop.metadata.CompressionCodecName;

public class EmpleadoParquetWriter {

    public static void main(String[] args) throws IOException {
        Schema schema = parseSchema();
        List<GenericData.Record> recordList = createRecords(schema);
        writeToParquetFile(recordList, schema);
    }

    private static Schema parseSchema() throws IOException {
        Schema.Parser parser = new Schema.Parser();
        return parser.parse(ParquetWriter.class.getResourceAsStream("/avro/empleado.avsc"));
    }

    private static List<GenericData.Record> createRecords(Schema schema) {
        List<GenericData.Record> recordList = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            GenericData.Record record = new GenericData.Record(schema);
            record.put("id", i);
            record.put("nombre", "Nombre"+i);
            record.put("apellidoPaterno", "Apellido Paterno"+i);
            record.put("apellidoMaterno", "Apellido Materno"+i);
            record.put("edad", 30+i);
            recordList.add(record);
        }
        return recordList;
    }

    private static void writeToParquetFile(List<GenericData.Record> recordList, Schema schema) throws IOException {
        Path path = new Path("/empleado.parquet");
        try (ParquetWriter<GenericData.Record> writer = AvroParquetWriter.
                    <GenericData.Record>builder(path)
                    .withRowGroupSize(ParquetWriter.DEFAULT_BLOCK_SIZE)
                    .withPageSize(ParquetWriter.DEFAULT_PAGE_SIZE)
                    .withSchema(schema)
                    .withConf(new Configuration())
                    .withCompressionCodec(CompressionCodecName.SNAPPY)
                    .withValidation(false)
                    .withDictionaryEncoding(false)
                    .build()){
            
            for (GenericData.Record record : recordList) {
                writer.write(record);
            }
        } 
    }
    
}
