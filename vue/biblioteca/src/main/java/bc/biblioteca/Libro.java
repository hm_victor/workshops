package bc.biblioteca;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;

/**
 *
 * @author Víctor
 */
@NamedQuery(
    name="Libros.getAll", 
    query="select b from Libro b"
)
@Entity
public class Libro implements Serializable {
    @Id
    private String isbn;
    private String titulo;
    private String autor;
    private String editorial;
    
    @ElementCollection
    @CollectionTable(
        name="libro_ubicacion",
        joinColumns = @JoinColumn(name="isbn")
    )
    private List<Ubicacion> ubicaciones=new LinkedList<>();

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public List<Ubicacion> getUbicaciones() {
        return ubicaciones;
    }

    public void setUbicaciones(List<Ubicacion> ubicaciones) {
        this.ubicaciones = ubicaciones;
    }
    
}
