package bc.biblioteca;

import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Víctor
 */
@Path("/libros")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class LibroResource {
    @Inject
    private LibroService libroService;
    
    @PUT
    @Path("/{isbn}")
    public void addOrUpdate(@PathParam("isbn") String isbn, Libro libro) {
        libroService.saveOrUpdate(libro);
    }
    
    @GET
    public List<Libro> getLibros() {
        return libroService.getLibros();
    }
    
    @DELETE
    @Path("/{isbn}")
    public void remover(@PathParam("isbn") String isbn) {
        libroService.remove(isbn);
    }
    
}
