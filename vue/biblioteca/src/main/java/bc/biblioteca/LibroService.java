package bc.biblioteca;

import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

/**
 *
 * @author Víctor
 */
@RequestScoped
public class LibroService {
    @Inject
    private EntityManager entityManager;

    public List<Libro> getLibros() {
        return entityManager.createNamedQuery("Libros.getAll", Libro.class).getResultList();
    }

    @Transactional
    public boolean remove(String isbn) {
        Libro libro = entityManager.find(Libro.class, isbn);
        if(libro == null) {
            return false;
        } else {
            entityManager.remove(libro);
            return true;
        }
    }

    @Transactional
    public void saveOrUpdate(Libro detached) {
        Libro managed = entityManager.find(Libro.class, detached.getIsbn());
        if(managed == null) {
            entityManager.persist(detached);
        } else { 
            managed.setTitulo(detached.getTitulo());
            managed.setAutor(detached.getAutor());
            managed.setEditorial(detached.getEditorial());
        }
    }
    
}
