jQuery(function ($) {
    let app = new Vue({
        el: "#app",
        data: {
            libros: [],
            nuevoLibro: {
                isbn: "",
                titulo: "",
                autor: "",
                editorial: "",
                ubicaciones: []
            },
            isbnLibroARemover: null
        },
        methods: {
            guardar: function () {
                fetch("http://localhost:8080/libros/" + this.nuevoLibro.isbn, {
                    method: "PUT",
                    body: JSON.toString(app.nuevoLibro)
                }).then(response => {
                    if (response.status === 200) {
                        this.obtenerLibros();
                    }
                });
            },
            obtenerLibros: function () {
                fetch("http://localhost:8080/libros").then(response => {
                    response.json().then(libros => {
                        app.libros = libros;
                    });
                });
            },
            mostrarDialogoLibro: function() {
                this.nuevoLibro={
                    isbn: "",
                    titulo: "",
                    autor: "",
                    editorial: "",
                    ubicaciones: [{
                        ubicacion: "ubicacion",
                        cantidad: 1
                    }]
                };
                $('#dialogoLibro').modal();
            },
            guardarLibro: function() {
                fetch("http://localhost:8080/libros/"+this.nuevoLibro.isbn, {
                        method: "PUT",
                        headers: {
                          'Accept': 'application/json',
                          'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(this.nuevoLibro),
                        contentType: "application/json"
                    }).then(response => {
                        if(response.status >= 200 && response.status < 300){
                            $('#dialogoLibro').modal("hide");
                            this.obtenerLibros();
                        }
                    });
            },
            agregarUbicacion: function() {
                this.nuevoLibro.ubicaciones.push({
                    ubicacion: "",
                    cantidad: 1
                });
            },
            removerUbicacion: function(indice) {
                this.nuevoLibro.ubicaciones.splice(indice, 1);
            },
            confirmarRemover: function(isbn) {
                this.isbnLibroARemover=isbn;
                $('#dialogoRemoverLibro').modal("show");
            },
            removerLibro: function() {
                fetch("http://localhost:8080/libros/"+this.isbnLibroARemover, {
                        method: "DELETE",
                        headers: {
                          'Accept': 'application/json',
                          'Content-Type': 'application/json'
                        },
                        contentType: "application/json"
                    }).then(response => {
                        if(response.status >= 200 && response.status < 300){
                            $('#dialogoRemoverLibro').modal("hide");
                            this.obtenerLibros();
                        }
                    });
            }
        }
    });

    app.obtenerLibros();
});