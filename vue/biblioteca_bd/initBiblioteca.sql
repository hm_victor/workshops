CREATE TABLE libro(
    isbn VARCHAR(20) PRIMARY KEY, 
    titulo VARCHAR(100) NOT NULL, 
    autor  VARCHAR(100) NOT NULL, 
    editorial VARCHAR(100) NOT NULL
);

CREATE TABLE libro_ubicacion(
    isbn VARCHAR(20),
    ubicacion VARCHAR(100),
    cantidad NUMERIC(10, 0),
    PRIMARY KEY(isbn, ubicacion),
    FOREIGN KEY(isbn) references libro(isbn)
);